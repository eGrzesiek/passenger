﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Passeger.Infrastructure.Services;
using Passeger.Infrastructure.DTO;

namespace Passenger.Api.Controllers
{
    [Route("[controller]")]
    public class DriversController : Controller
    {
        private readonly IDriverService _driverService;

        public DriversController(IDriverService driverService)
        {
            _driverService = driverService;
        }

        [HttpGet("{name}")]
        public DriverDto Get(string name)
        => _driverService.GetByName(name);
    }
}
