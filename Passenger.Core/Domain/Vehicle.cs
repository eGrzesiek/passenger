﻿using System;

namespace Passenger.Core.Domain
{
    public class Vehicle
    {
        public string Brand { get; protected set; }
        public string Name { get; protected set; }
        public int Seats{ get; protected set; }
        


        public Vehicle(string brand, string name, int seats)
        {
            SetBrand(brand);
            SetName(name);
            SetSeats(seats);
        }

        private void SetBrand(string brand)
        {
            if (string.IsNullOrWhiteSpace(brand))
            {
                throw new Exception("Dodaj poprawne dane");
            }
            if (Brand == brand)
            {
                return;
            }

            Brand = brand;
        }
        private void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Dodaj poprawne dane");
            }
            if (Name == name)
            {
                return;
            }

            Name = name;
        }
        private void SetSeats(int seats)
        {
            if (seats<=1)
            {
                throw new Exception("Dodaj poprawne dane");
            }
            if (Seats == seats)
            {
                return;
            }

            Seats = seats;
        }

        public static Vehicle Create(string brand, string name, int seats)
        {
            return new Vehicle(brand, name, seats);
        }
    }
}