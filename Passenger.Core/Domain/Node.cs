﻿namespace Passenger.Core.Domain
{
    public class Node
    {

        public string Address { get; protected set; }
        public double Lognitude { get; protected set; }
        public double Latitude { get; protected set; }
    }
}