﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Passenger.Core.Domain
{
    public class Passenger
    {
        public Guid Id { get; protected set; }
        public Guid UserId { get; protected set; }
        public Vehicle Vehicle{ get; protected set; }
        public IEnumerable<Route> Routes { get; protected set; }
        public Node Address { get; protected set; }
        public IEnumerable<Trip> Trips { get; protected set; }
    }
}
