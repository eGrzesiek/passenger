﻿using Passenger.Core.Domain;
using Passenger.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Passeger.Infrastructure.DTO;
using AutoMapper;

namespace Passeger.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _mapper = mapper;
            _userRepository = userRepository; 
        }

        public UserDto Get(string email)
        {
            var u = _userRepository.Get(email);
            return _mapper.Map<User, UserDto>(u);
            

        }


        public void Register(string email, string username,  string password)
        {
            var user = _userRepository.Get(email);
            if (user != null)
            {
                throw new Exception("juz istnieje");
            }

            var salt = Guid.NewGuid().ToString("N");
            user = new User(email,username, password,salt);
            _userRepository.Add(user);

        }
    }
}
