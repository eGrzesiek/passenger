﻿using Passeger.Infrastructure.DTO;
using Passenger.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Passeger.Infrastructure.Services
{
    public class DriverService : IDriverService
    {

        private readonly IDriverRepository _driverRepository;

        public DriverService(IDriverRepository driverRepository)
        {
            _driverRepository = driverRepository;
        }

        public DriverDto Get(Guid id)
        {
            var driver = _driverRepository.Get(id);

            return new DriverDto
            {
                //id 
            };
        }

        public DriverDto GetByName(string name)
        {
            var driver = _driverRepository.GetByName(name);
            return new DriverDto
            {
                Id = driver.Id,
                Name = driver.Name
            };
        }
    }
}
