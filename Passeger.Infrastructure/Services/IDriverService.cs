﻿using Passeger.Infrastructure.DTO;
using System;

namespace Passeger.Infrastructure.Services
{
    public interface IDriverService
    {
        DriverDto Get(Guid id);
        DriverDto GetByName(string name);
    }
}