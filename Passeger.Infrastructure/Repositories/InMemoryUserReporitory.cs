﻿using Passenger.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Passenger.Core.Domain;
using System.Linq;

namespace Passeger.Infrastructure.Repositories
{
    public class InMemoryUserReporitory : IUserRepository

    {
        private static ISet<User> _users = new HashSet<User>() {
            new User("user10@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user11@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user12@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user13@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user14@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user15@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user16@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user17@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user18@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user19@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user20@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user21@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
            new User("user22@czysto.pl", Guid.NewGuid().ToString(),Guid.NewGuid().ToString(),"asdasdadasdasd"),
        };


        public void Add(User user)
        {
            _users.Add(user);
        }

        public User Get(Guid id)
        => _users.SingleOrDefault(x => x.Id == id);

        public User Get(string email)
        => _users.SingleOrDefault(x => x.Email == email.ToLowerInvariant());

        public IEnumerable<User> GetAll()
        => _users;

        public void Remove(Guid id)
        {
            var user = Get(id);
            _users.Remove(user);
        }

        public void Update(User user)
        {
             
        }
    }
}
