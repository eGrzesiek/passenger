﻿using Passenger.Core.Domain;
using Passenger.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Passeger.Infrastructure.Repositories
{
    public class InMemoryDriverRepository : IDriverRepository
    {
        private static ISet<Driver> _drivers = new HashSet<Driver>()
        {
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"jeden" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"dwa" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"trzy" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"cztery" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"piec" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"szesc" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"siedem" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"osiem" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"dziewiec" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"dziesiec" ),
            new Driver(Guid.NewGuid(),Guid.NewGuid(),"jedenascie" ),
        };

        public void Add(Driver driver)
        {
            _drivers.Add(driver);

        }

        public Driver Get(Guid id)
        {
            return _drivers.Single(x => x.Id == id);
        }

        public Driver GetByName(string name)
        {
            return _drivers.Single(x => x.Name == name);
        }

        public IEnumerable<Driver> GetAll()
        => _drivers;

        public void Update(Driver driver)
        {
            return; 
        }
    }
}
