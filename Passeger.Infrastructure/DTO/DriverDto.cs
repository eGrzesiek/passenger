﻿using Passenger.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Passeger.Infrastructure.DTO
{
    public class DriverDto
    {
        public Guid Id { get;  set; }
        public Vehicle Vehicle { get;  set; }
        public IEnumerable<Route> Routes { get;  set; }
        public IEnumerable<Trip> DailyRoutes { get;  set; }
        public string Name { get; set; }
    }
}

