﻿using AutoMapper;
using Passeger.Infrastructure.DTO;
using Passenger.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Passeger.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
        {
            var conf = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<User, UserDto>();
                    cfg.CreateMap<Driver, DriverDto>();
                }
            )
            .CreateMapper();

            return conf;
        }
    }
}
